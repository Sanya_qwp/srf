import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import Firebase from 'firebase';
import VueAxios from 'vue-axios';
import { firestorePlugin } from 'vuefire';

import App from './App';
import router from './router';

import 'normalize.css';
import 'bootstrap/dist/css/bootstrap.css';

import './firebase/db.js';
import { store } from './store/store.js';

Vue.use(Vuex);
Vue.use(Firebase);
Vue.use(firestorePlugin);
Vue.use(VueAxios, axios);

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
