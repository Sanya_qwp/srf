import firebase from 'firebase'

const app = firebase.initializeApp({
  apiKey: "AIzaSyD4CV0or8hGRbRUtJwHFrV_ohh95Mv9F9k",
  authDomain: "student-randomizer-3de13.firebaseapp.com",
  databaseURL: "https://student-randomizer-3de13.firebaseio.com",
  projectId: "student-randomizer-3de13",
  storageBucket: "student-randomizer-3de13.appspot.com",
  messagingSenderId: "867808228289",
  appId: "1:867808228289:web:0c43160c96fe01a9173540",
  measurementId: "G-5KCQ9MB3S1"
});

export const db = firebase.firestore();