export const toggleBtn = { 
  toShowId: 'addStudentForm',
  toShowClass: 'student__add-form',
  clicked(event) {
    const formShowToggle = document.getElementById(this.toShowId);
    formShowToggle.classList.toggle(this.toShowClass);
  }
}