import Vue from 'vue'
import Router from 'vue-router'

//PAGE IMPORTS
import StudentsList from './components/PageStudentsList.vue'
import NotFound from './components/PageNotFound.vue'

//DEV IMPORTS
import ComponentsList from './components/PageComponentsList.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    //DEV URLS!
    {
      path: '/',
      name: 'componentslist',
      component: StudentsList
    },
    //end of dev urls!

    {
      path: '*',
      name: 'pagenotfound',
      component: NotFound
    }
  ]
})
